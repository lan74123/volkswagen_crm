<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('origin_id', 20);
            $table->string('img_path', 100)->nullable();
            $table->string('name', 100)->nullable();    
            $table->string('email', 100)->nullable();
            $table->string('password', 200)->nullable();
            $table->string('mobile', 20)->nullable();
            $table->string('address', 100)->nullable();
            $table->integer('agree_data_use')->default(0);
            $table->string('authentication_code', 10)->nullable();
            $table->integer('is_authenticated')->default(0);
            $table->text('device_id')->nullable();
            $table->integer('valid')->default(0);
            $table->timestamps();
            $table->integer('oid')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
