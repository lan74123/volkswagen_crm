<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('message_groups_id');
            $table->integer('member_id');
            $table->integer('valid')->default(1);
            $table->timestamps();
            $table->integer('oid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_members');
    }
}
