<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DevicePush extends Model
{
    protected $table = 'device_pushs';
    
    public function member()
    {
        return $this->hasOne('App\Member', 'id', 'member_id');
    }
}
