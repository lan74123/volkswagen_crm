<?php

namespace App\Http\Middleware;

use Closure;

class CheckJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $paremeter = $request->query('token') ;
        $headers = $request->headers->all();
        $authorization = '';

        foreach($headers as $key => $value)
        {
            if($key == 'authorization')
            {            
                if(!is_null($value))
                {       
                    $authorization = $value[0];
                }
            }
        }
        
        $result = 'Y';
        $msg = '';
        
        //檢查header 包含Bearer token
        if(is_null($authorization) && is_null($paremeter))
        {
            $result = 'N';
            $msg = 'token 無效';
        }

        if(!is_null($authorization) && is_null($paremeter))
        {            
            if (strpos(strtolower($authorization),'bearer ') !== false) 
            {
               
            }
            else
            {
                $result = 'N';
                $msg = 'token 無效';
            }
        }
        
        if($msg != '')
        {
            return response()->json([
                'Result' => $result,
                'ErrorCode' => '',
                'ErrorMsg' => $msg                
            ],200);
        }
        else
        {
            return $next($request);
        }       
    
    }
}
