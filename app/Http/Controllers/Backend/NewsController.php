<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services as Service;


/**
 * NewsController class
 * 
 */
class NewsController extends Controller
{   
    //News Service
    protected $newsService;
    
    /**
    * 建構子
    *
    * @param Service\Common\NewsService $newsService
    */
    public function __construct(Service\Common\NewsService $newsService)
    {
        $this->newsService = $newsService;
    }

    /**
    * News列表頁
    *
    * @param Request $request
    * @return void
    */
    public function index(Request $request)
    {  
        //預設有效
        if ( $_SERVER['REQUEST_METHOD'] == "GET" )
        {
            $request->valid = '1';
        }
        
        $tables = $this->newsService->searchNews($request,15);

        return view('admin.news.list', [
            'tables' => $tables,
            'cond' => $request,
        ]);
    }

    /**
    * News新增頁
    *
    * @param Request $request
    * @return void
    */
    public function create()
    {
        return view('admin.news.create',[
            'news' => "",
        ]);
    }

    /**
     * News修改頁
     *
     * @param integer $id
     * @return void
     */
    public function edit($id = 0)
    {
        $news = $this->newsService->getNews($id);

        return view('admin.news.edit',[
            'news' => $news,
        ]);
    }

    /**
     * News新增
     *
     * @param Request $request
     * @return void
     */
    public function insert(Request $request)
    {  
        $this->newsService->insertNews($request);

        return redirect('/backend/news');      
    }

    /**
     * News修改
     *
     * @param Request $request
     * @return void
     */
    public function modify(Request $request)
    {   
        $id = $this->newsService->modifyNews($request);

        return redirect('/backend/news/edit/'.$id);
    }

    /**
     * News刪除
     *
     * @param Request $request
     * @return void
     */
    public function delete($id = 0)
    {      
        $this->newsService->deleteNews($id);
        
        return redirect('/backend/news');
    }
}
