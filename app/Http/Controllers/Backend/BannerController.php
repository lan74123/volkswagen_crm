<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Banner;
use Illuminate\Http\Request;
use App\Services as Service;


class BannerController extends Controller
{
    //Banner Service
    protected $bannerService;
    
    /**
    * 建構子
    *
    * @param Service\Backend\BannerService $bannerService
    */
    public function __construct(Service\Backend\BannerService $bannerService)
    {
        $this->bannerService = $bannerService;       
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $tables = $this->bannerService->searchBanner($request,15);

        return view('admin.banner.list', [
            'tables' => $tables,
            'cond' => $request,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banner.create',[
            'banner' => "",
        ]);
    }

    /**
     * Insert a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        $this->bannerService->insertBanner($request);
        return redirect('/backend/banner');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit($id = 0)
    {
        $banner = $this->bannerService->getBanner($id);

        return view('admin.banner.edit',[
            'banner' => $banner,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function modify(Request $request)
    {
        $id = $this->bannerService->modifyBanner($request);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function delete($id = 0)
    {
        $this->bannerService->deleteBanner($id);
        return redirect('/backend/banner');
    }
}
