<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services as Service;


/**
 * MessageGroupController class
 * 
 */
class MessageGroupController extends Controller
{   
    //訊息群組Service
    protected $messageGroupService, $excelService, $emailService, $memberService;
    
    /**
     * 建構子
     *
     * @param Service\Backend\MessageGroupService $messageGroupService
     * @param Service\Common\ExcelService $excelService
     * @param Service\Common\EmailService $emailService
     */
    public function __construct(Service\Backend\MessageGroupService $messageGroupService, 
    Service\Common\ExcelService $excelService, 
    Service\Common\EmailService $emailService,
    Service\Common\MemberService $memberService)
    {
        $this->messageGroupService = $messageGroupService;
        $this->excelService = $excelService;
        $this->emailService = $emailService;
        $this->memberService = $memberService;
    }

    /**
    * 訊息群組列表頁
    *
    * @param Request $request
    * @return void
    */
    public function index(Request $request)
    {  
        $tables = $this->messageGroupService->searchMessageGroup($request,15);

        return view('admin.messageGroup.list', [
            'tables' => $tables,
            'cond' => $request,
        ]);
    }
    
    /**
    * 訊息群組篩選會員
    *
    * @param Request $request
    * @return void
    */
    public function filterMemberList(Request $request)
    { 
        $tables = $this->memberService->searchMemberByGroup($request, 15);

        return view('admin.messageGroup.filterMemberList',[
            'messageGroup' => "",
            'tables' => $tables,
            'members' => count($tables),
            'cond' => $request,
        ]);
    }

    /**
    * 訊息群組新增頁
    *
    * @param Request $request
    * @return void
    */
    public function create(Request $request)
    {
        $filter['name'] = $request->name;
        $filter['mobile'] = $request->mobile;
        $filter['last_maintenance'] = $request->last_maintenance;
        $filter['car_model'] = $request->car_model;
        $filter['mileage'] = $request->mileage; 
        $filter['date_start'] = $request->date_start;
        $filter['date_end'] = $request->date_end;

        return view('admin.messageGroup.create',[
            'messageGroup' => "", 
            "filterCond" => $filter,           
        ]);
    }
    /**
     * 訊息群組新增
     *
     * @param Request $request
     * @return void
     */
    public function insert(Request $request)
    {
        $id = $this->messageGroupService->insertMessageGroup($request);

        $name = $request->filter_name;
        $mobile = $request->filter_mobile;     
        $car_model = $request->filter_car_model;
        $last_maintenance = $request->filter_last_maintenance;
        $mileage = $request->filter_mileage;     
        $date_start = $request->filter_date_start;
        $date_end = $request->filter_date_end;     

        //member cond
        // $request = new Request();
        $request->name = $name;
        $request->mobile = $mobile;
        $request->car_model = $car_model;
        $request->mileage = $mileage;
        $request->date_start = $date_start;
        $request->date_end = $date_end;

        //find member
        $member_list = $this->memberService->searchMemberByGroup($request, 0);

        foreach ($member_list as $member)
        {           
            $this->messageGroupService->insertMessageGroupMember($id, $member->id);
        }
        
        return redirect('/backend/messageGroup');      
    }

    
    /**
     * 訊息群組修改頁
     *
     * @param integer $id
     * @return void
     */
    public function edit($id = 0)
    {

        if($id == 0)
        {
            return redirect('/backend/messageGroup');
        }

        $messageGroup = $this->messageGroupService->getMessageGroup($id);

        $messageGroupMember = $this->messageGroupService->getMessageGroupMember(15, $id);


        return view('admin.messageGroup.edit',[
            'messageGroup' => $messageGroup,
            'messageGroupMember' => $messageGroupMember,
        ]);
    }

    /**
     * 訊息群組修改
     *
     * @param Request $request
     * @return void
     */
    public function modify(Request $request)
    {   
        $id = $this->messageGroupService->modifyMessageGroup($request);

        return redirect('/backend/messageGroup/edit/'.$id);
    }

    /**
     * 訊息群組刪除
     *
     * @param Request $request
     * @return void
     */
    public function delete($id = 0)
    {      
        //刪除群組
        $this->messageGroupService->deleteMessageGroup($id);
        //刪除群組名單
        $this->messageGroupService->deleteMessageGroupMemberByGroup($id);

        return redirect('/backend/messageGroup');
    }

    /**
     * 訊息群組名單刪除
     *
     * @param Request $request
     * @return void
     */
    public function deleteMember($group_id = 0, $id = 0)
    {      
        $this->messageGroupService->deleteMessageGroupMember($id);
        
        return redirect('/backend/messageGroup/edit/' . $group_id);
    }
    

    /**
     * 訊息群組資料下載
     *
     * @return void
     */
    public function download($id = 0, Request $request)
    {         
        try
        {          
            $messageGroup = $this->messageGroupService->getMessageGroup($id);

            $tables =  $this->messageGroupService->getMessageGroupMember(0, $id);
           
            $titles=[
                '姓名',
                'Email',
                '連絡電話',
                '連絡地址',
                '同意資料使用',
                '註冊日期',
                '狀態'
            ];
            
            $filename = '';

            $datas=array();
            
            foreach($tables as $row)
            {           
                $filename = $row->messageGroup->name;

                $rowData=array();
                array_push($rowData,$row->member->name);
                array_push($rowData,$row->member->email);
                array_push($rowData,$row->member->mobile);
                array_push($rowData,$row->member->address);
                array_push($rowData,( $row->member->agree_data_use )? 'Y':'N');
                array_push($rowData,$row->member->created_at);
                array_push($rowData,( $row->member->valid )? '有效':'無效');           
                array_push($datas,$rowData);
            }      

            $this->excelService->downloadExcel($titles, $datas, $filename . '_' . date("Y-m-d-h:i:s",time()));  

        }
        catch(Exception $e)
        {
            return redirect('/backend/messageGroup/edit' . $id);
        }
    }    
}
