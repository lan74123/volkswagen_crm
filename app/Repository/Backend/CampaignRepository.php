<?php

namespace App\Repository\Backend;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Illuminate\Http\Request;
use App\Campaign;
use Auth;
use App;
use DB;
use Image;

class CampaignRepository
{
    /**
     * 建構子
     */
    public function __construct()
    {
        if (!file_exists(base_path('storage/app/public/campaign/')))
        {
            mkdir(base_path('storage/app/public/campaign/'));
        }
    }

    /**
     * 取得所有活動
     *
     * @return Campaign
     */
    public function getAllCampaigns()
    {
        return Campaign::orderBy('created_at', 'DESC')->get();
    }

    /**
     * 取得活動
     *
     * @param integer $id
     * @return Campaign
     */
    public function getCampaign($id = 0)
    {
        return Campaign::find($id);
    }

    /**
     * 修改活動
     *
     * @param Request $request
     * @param integer $id
     * @return void
     */
    public function modifyCampaign(Request $request, $id = 0)
    {
        $campaign = $this->getCampaign($id);

        $campaign->name = $request->name;
        $campaign->place = $request->place;
        $campaign->type = $request->type;
        $campaign->start_date = $request->start_date;
        $campaign->end_date = $request->end_date;
        $campaign->description = $request->description;
        $campaign->valid = ($request->valid == 'on') ? 1 : 0;
        $campaign->oid = Auth::user()->id;

        if ($request->hasFile('image'))
        {
            $fileName = $id . '_' . date('YmdHis') . '_' . Uuid::uuid4()->toString() . '.jpg';
            $image = Image::make($request->image);
            $image->save(base_path('storage/app/public/campaign/') . $fileName);

            $campaign->image = $fileName;
        }

        $campaign->save();
    }

    /**
     * 新增活動
     *
     * @param Request $request
     * @return void
     */
    public function insertCampaign(Request $request)
    {
        $campaign = new Campaign();
        $campaign->name = $request->name;
        $campaign->place = $request->place;
        $campaign->type = $request->type;
        $campaign->start_date = $request->start_date;
        $campaign->end_date = $request->end_date;
        $campaign->description = $request->description;
        $campaign->valid = ($request->valid == 'on') ? 1 : 0;
        $campaign->oid = Auth::user()->id;
        $campaign->save();

        if ($request->hasFile('image'))
        {
            $fileName = $campaign->id . '_' . date('YmdHis') . '_' . Uuid::uuid4()->toString() . '.jpg';
            $image = Image::make($request->image);
            //FIXME:如果要用要修這裡的儲存
            $image->save(base_path('storage/app/public/campaign/') . $fileName);

            $campaign->image = $fileName;
            $campaign->save();
        }
    }

    /**
     * 刪除活動
     *
     * @param integer $id
     * @return bool
     */
    public function deleteCampaign($id = 0)
    {
        $campaign = $this->getCampaign($id);
        $campaign->delete();

        return true;
    }
}
