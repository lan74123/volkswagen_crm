<?php

namespace App\Repository\Backend;

use Illuminate\Http\Request;
use App\Services as Service;
use App\Message;
use App\MessageMember;
use DateTime;
use Auth;
use App;
use DB;
use Image;

class MessageRepository
{

    protected $fileService;

    public function __construct(Service\Common\FileService $fileService)
    {
        $this->fileService = $fileService;       
    }

    /**
    * 搜尋多筆訊息群組
    *
    * @param integer $pageLimit
    * @param [type] $area
    * @param [type] $name
    * @param [type] $tel
    * @param [type] $valid
    * @return void
    */
    public function searchMessages($pageLimit = 0, $name = null, $date_start = '', $date_end = '', $valid = null)
    {        
        $Message_All = Message::select('*');      

        if(!is_null($name))
		{			
            $Message_All->where('title', 'LIKE', '%' . $name . '%');
        }

        if(!empty($date_start))
		{
             $Message_All->where('created_at','>=', $date_start);
		}
        
        if(!empty($date_end))
		{
             $Message_All->where('created_at','<=', $date_end);
		}
      
        $Message_All->where('valid', 1);          

        $Message_All->orderBy('id', 'desc');

        //GetAll
        if( $pageLimit == 0 )
        {         
            $Message_List = $Message_All->get();
        }
        else
        {
            $Message_List = $Message_All->paginate($pageLimit);
        }
            
        return $Message_List;       
    }

    /**
     * 取得單筆訊息群組By ID
     *
     * @param integer $id
     * @return void
     */
    public function getMessage($id = 0)
    {
        $Message = Message::find($id);

        return $Message;      
    }  

     /**
     * 新增訊息
     *
     * @param integer $id
     * @return void
     */
    public function insertMessage(Request $request, $img_name = '')
    {
        $message = new Message;
        
        $message->message_groups_id = $request->message_groups_id;
        $message->member_id = $request->member_id;
        $message->type = $request->sendTimeType;
        $send_time = '';

        if( $request->sendTimeType == 'RESERVE')
        {
            $date = $request->sendDate;
            $hour = $request->sendHour;
            $min1 = $request->minutes_1;
            $min2 = $request->minutes_2;

            $time = $date .' '. $hour . ':' . $min1 . $min2 .':00';

            $send_time = DateTime::createFromFormat('Y-m-d H:i:s', $time)->format('Y-m-d H:i:s');
        }
        else
        {
            $send_time = date('Y-m-d H:i:s', strtotime("+5 min"));
        }
        $message->send_time = $send_time;
        $message->title = $request->title;       
        $message->content = $request->content; 
        $message->url = $request->url; 
        $message->news_id = $request->news_id; 
        $message->valid = 1;       
        $message->oid = Auth::user()->id;        

        $message->save();
        $img_path = '';
        //共用圖片 不用寫入相同檔案
        if(!is_null($request->img_path_url))
        {
            $message->img_path = $request->img_path_url;
            $message->save();
            $img_path = $request->img_path_url;
            return $img_path;
        }


        if ($request->hasFile('img_path'))
        {
            $fileName = $img_name . '.jpg';
            $image = Image::make($request->file('img_path')->getRealPath());
            $folder_name = 'message';
            $this->fileService->saveFile($folder_name, $image->stream(), $fileName);
            $message->img_path = $fileName;
            $message->save();

            $img_path = $fileName;
        }

        return $img_path;
    }   

    function deleteMessage($id = 0)
    {
        $message = Message::find($id);
        $message->valid = '0';
        $message->save();
    }

    /**
     * 取得訊息清單By Member_ID
     *  
     * @param integer $message_groups_id
     * @return void
     */
    public function getMessageByMember($pageLimit = 0, $member_id = 0)
    {
        $MessageMember_All = Message::select('*'); 
        
        $MessageMember_All->where('member_id', $member_id);
        $MessageMember_All->where('valid', 1);
        $MessageMember_All->where('send_status', 1);
        //GetAll
        if( $pageLimit == 0 )
        {         
            $MessageMember_All->where('send_status', 1);
            $MessageMember_List = $MessageMember_All->get();    
        }
        else
        {
            $MessageMember_List = $MessageMember_All->paginate($pageLimit);
        }

        return $MessageMember_List;
    }

    /**
     * 搜尋訊息By ID 更新已讀狀況
     *
     * @param integer $id
     * @param integer $member_id
     * @return void
     */
    public function findMessageByID($id = 0, $member_id = 0)
    {
        $message = Message::find($id);
        $message->status = 1;
        $message->save();        
        return $message;
    }

    /**
     * FCM發送推播
     *
     * @param [type] $date_now
     * @return void
     */
    public function findPreSendMessage($date_now)
    {
        $MessageMember_Send = DB::table('messages')
        ->selectRaw('messages.id, message_groups.name as groupName, members.name as mbName,
        messages.title, messages.content, messages.news_id , messages.send_status ,
        device_pushs.push_token, device_pushs.device, 
        messages.id mbId')
        ->leftJoin('message_groups', 'messages.message_groups_id', '=', 'message_groups.id')
        ->leftJoin('members', 'messages.member_id', '=', 'members.id')
        ->leftJoin('device_pushs', 'messages.member_id', '=', 'device_pushs.member_id')
        ->where('send_time','<=', $date_now)
        ->where('send_status', '0')
        ->orderBy('id', 'asc')
        ->get();
        
        return $MessageMember_Send;
    }

    /**
     * FCM發送推播
     *
     * @param [type] $date_now
     * @return void
     */
    public function findPreSendMessageById($id = 0)
    {
        $MessageMember_Send = DB::table('messages')
        ->selectRaw('messages.id, message_groups.name as groupName, members.name as mbName,
        messages.title, messages.content, messages.news_id , messages.send_status ,
        device_pushs.push_token, device_pushs.device, 
        messages.id mbId')
        ->leftJoin('message_groups', 'messages.message_groups_id', '=', 'message_groups.id')
        ->leftJoin('members', 'messages.member_id', '=', 'members.id')
        ->leftJoin('device_pushs', 'messages.member_id', '=', 'device_pushs.member_id')
        ->where('messages.id','=', $id)        
        ->orderBy('id', 'asc')
        ->get();
        
        return $MessageMember_Send;
    }

    /**
     * 訊息發送成功
     *
     * @param integer $id
     * @return void
     */
    public function sendMessageSuccess($id = 0)
    {
        $message = Message::find($id);
        $message->send_status = 1;
        $message->save(); 
        
        return $message;
    }
}
