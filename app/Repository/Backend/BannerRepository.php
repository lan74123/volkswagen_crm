<?php

namespace App\Repository\Backend;

use Illuminate\Http\Request;
use App\Banner;
use Auth;
use App;
use DB;
use Illuminate\Support\Facades\Storage;



class BannerRepository
{

    /**
    * 搜尋多筆經銷商
    *
    * @param integer $pageLimit
    * @param [type] $banner
    * @param [type] $device
    * @param [type] $url
    * @param [type] $is_update
    * @return void
    */
    public function searchBanner($pageLimit = 0, $title = null)
    {        
        $Banner_All = Banner::select('*');

        if(!is_null($title))
		{			
            $Banner_All->where('title', 'LIKE', '%' . $title . '%');
        }

        $Banner_All->orderBy('id', 'desc');

        //GetAll
        if( $pageLimit == 0 )
        {         
            $Banner_List = $Banner_All->get();
        }
        else
        {
            $Banner_List = $Banner_All->paginate($pageLimit);
        }
            
        return $Banner_List;       
    }

    /**
     * 取得單筆經銷商By ID
     *
     * @param integer $id
     * @return void
     */
    public function getBanner($id = 0)
    {
        $banner = Banner::find($id);

        return $banner;      
    }  

     /**
     * 新增經銷商
     *
     * @param integer $id
     * @return void
     */
    public function insertBanner(Request $request)
    {
        $banner = new Banner;
        $banner->title = $request->title;
        $banner->banner = Storage::disk('public')->putFile('/banner', $request->banner);
        $banner->save();

        return $banner->id;
    }

    /**
     * 修改經銷商資料
     *
     * @param integer $id
     * @return void
     */
    public function modifyBanner(Request $request)
    {
        $banner = Banner::find($request->id);
        $banner->title = $request->title;
        if ($request->banner)
        {
            Storage::disk('public')->putFileAs('', $request->banner, $banner->banner);
        }
        $banner->save();

        return $banner->id;
    }

    /**
     * 刪除經銷商
     *
     * @param integer $id
     * @return void
     */
    public function deleteBanner($id = 0)
    {
        $banner = Banner::find($id);
        $path = str_replace("/storage","/public",$banner->banner);
        Storage::delete($path);
        $banner->delete();
    }
}
