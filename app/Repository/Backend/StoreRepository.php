<?php

namespace App\Repository\Backend;

use Illuminate\Http\Request;
use App\Store;
use Auth;
use App;
use DB;


class StoreRepository
{

    /**
    * 搜尋多筆商店
    *
    * @param integer $pageLimit
    * @param [type] $area
    * @param [type] $name
    * @param [type] $tel
    * @param [type] $valid
    * @return void
    */
    public function searchStores($pageLimit = 0, $dealer_id = null, $area = null, $type = null, $name = null, $tel = null, $valid = null)
    {        
        $Store_All = Store::select('*');

        
        if(!is_null($dealer_id))
		{		
            if($dealer_id!=0)
            {	
                $Store_All->where('dealer_id', $dealer_id);
            }
        }

        if(!is_null($area))
		{			
            $Store_All->where('area', 'LIKE', '%' . $area . '%');
        }

        if(!is_null($type))
		{			
            $Store_All->where('type', $type);
        }

        if(!is_null($name))
		{			
            $Store_All->where('name', 'LIKE', '%' . $name . '%');
        }
        
        if(!is_null($tel))
		{			
            $Store_All->where('tel', 'LIKE', '%' . $tel . '%');
        }     
    
        if(!is_null($valid))
		{           
            if($valid != "")//FOR 不拘
            {
                $Store_All->where('valid', $valid);
            }
		}		

        $Store_All->orderBy('id', 'desc')->orderBy('valid', 'desc');

        //GetAll
        if( $pageLimit == 0 )
        {         
            $Store_List = $Store_All->get();
        }
        else
        {
            $Store_List = $Store_All->paginate($pageLimit);
        }
            
        return $Store_List;       
    }

    /**
     * 取得單筆商店By ID
     *
     * @param integer $id
     * @return void
     */
    public function getStore($id = 0)
    {
        $Store = Store::find($id);

        return $Store;      
    }  

     /**
     * 新增商店
     *
     * @param integer $id
     * @return void
     */
    public function insertStore(Request $request)
    {
        $store = new Store;
        $store->dealer_id = $request->dealer;
        $store->area = $request->area;
        $store->type = $request->type;
        $store->name = $request->name;
        $store->code = $request->code;        
        $store->tel = $request->tel;
        $store->fax = $request->fax;
        $store->address = $request->address;
        $store->email = $request->email; 
        $store->emailcc = $request->emailcc;
        $store->servicetime = $request->servicetime;     
        $store->valid = ($request->valid == 'on') ? 1 : 0;       
        $store->oid = Auth::user()->id;        

        $store->save();

        $id = $store->id;

        return $id;
    }

    /**
     * 修改商店資料
     *
     * @param integer $id
     * @return void
     */
    public function modifyStore(Request $request)
    {
        $store = Store::find($request->id);
        $store->dealer_id = $request ->dealer;
        $store->area = $request->area;
        $store->type = $request->type;
        $store->name = $request->name;
        $store->code = $request->code;        
        $store->tel = $request->tel;
        $store->fax = $request->fax;
        $store->address = $request->address;
        $store->email = $request->email;  
        $store->emailcc = $request->emailcc;
        $store->servicetime = $request->servicetime;
        $store->valid = ($request->valid == 'on') ? 1 : 0;
        $store->oid = Auth::user()->id;        

        $store->save(); 

        return $request->id;
    }

    /**
     * 刪除商店
     *
     * @param integer $id
     * @return void
     */
    public function deleteStore($id = 0)
    {
        $store = Store::find($id);   
        $store->valid = 0;
        $store->oid = Auth::user()->id;        

        $store->save();      
        //$store->delete();
    }
}
