<?php

namespace App\Repository\Backend;

use Illuminate\Http\Request;
use App\Version;
use Auth;
use App;
use DB;


class VersionRepository
{

    /**
    * 搜尋多筆經銷商
    *
    * @param integer $pageLimit
    * @param [type] $version
    * @param [type] $device
    * @param [type] $url
    * @param [type] $is_update
    * @return void
    */
    public function searchVersions($pageLimit = 0, $version = null, $device = null, $url = null, $is_update = null)
    {        
        $Version_All = Version::select('*');

        if(!is_null($version))
		{			
            $Version_All->where('version', 'LIKE', '%' . $version . '%');
        }

        if(!is_null($device))
		{			
            $Version_All->where('device', 'LIKE', '%' . $device . '%');
        }
        
        if(!is_null($url))
		{			
            $Version_All->where('url', 'LIKE', '%' . $url . '%');
        }     
    
        if(!is_null($is_update))
		{           
            if($is_update != "")//FOR 不拘
            {
                $Version_All->where('is_update', $is_update);
            }
		}		

        $Version_All->orderBy('id', 'desc');

        //GetAll
        if( $pageLimit == 0 )
        {         
            $Version_List = $Version_All->get();
        }
        else
        {
            $Version_List = $Version_All->paginate($pageLimit);
        }
            
        return $Version_List;       
    }

    /**
     * 取得單筆經銷商By ID
     *
     * @param integer $id
     * @return void
     */
    public function getVersion($id = 0)
    {
        $version = Version::find($id);

        return $version;      
    }  

     /**
     * 新增經銷商
     *
     * @param integer $id
     * @return void
     */
    public function insertVersion(Request $request)
    {
        $version = new Version;
        $version->version = $request->version;
        $version->device = $request->device;
        $version->is_update = ($request->is_update == 'on') ? 1 : 0;        
        $version->message = $request->message;
        $version->url = $request->url;
        $version->oid = Auth::user()->id;        

        $version->save();

        return $version->id;
    }

    /**
     * 修改經銷商資料
     *
     * @param integer $id
     * @return void
     */
    public function modifyVersion(Request $request)
    {
        $version = Version::find($request->id);
        $version->version = $request->version;
        $version->device = $request->device;
        $version->is_update = ($request->is_update == 'on') ? 1 : 0;        
        $version->message = $request->message;
        $version->url = $request->url;
        $version->oid = Auth::user()->id;        

        $version->save();

        return $version->id;
    }

    /**
     * 刪除經銷商
     *
     * @param integer $id
     * @return void
     */
    public function deleteVersion($id = 0)
    {
        $dealer = Version::find($id);        
        $dealer->delete();
    }

    /**
     * API 依裝置取得最新版本
     * 
     * @param string $device
     * @return void
     */
    public function getLatestVersion($device = null)
    {
        return Version::where('device', $device)->orderBy('created_at', 'desc')->first();
    }
}
