<?php

namespace App\Repository\Backend;

use Illuminate\Http\Request;
use App\MessageGroup;
use App\MessageMember;
use Auth;
use App;
use DB;


class MessageGroupRepository
{
    /**
    * 搜尋多筆訊息群組
    *
    * @param integer $pageLimit
    * @param [type] $area
    * @param [type] $name
    * @param [type] $tel
    * @param [type] $valid
    * @return void
    */
    public function searchMessageGroups($pageLimit = 0, $name = null, $date_start = '', $date_end = '', $valid = null)
    {        
        $MessageGroup_All = MessageGroup::select('*');      

        if(!is_null($name))
		{			
            $MessageGroup_All->where('name', 'LIKE', '%' . $name . '%');
        }

        if(!empty($date_start))
		{
             $MessageGroup_All->where('created_at','>=', $date_start);
		}
        
        if(!empty($date_end))
		{
             $MessageGroup_All->where('created_at','<=', $date_end);
		}
      
        $MessageGroup_All->where('valid', 1);          

        $MessageGroup_All->orderBy('id', 'desc');

        //GetAll
        if( $pageLimit == 0 )
        {         
            $MessageGroup_List = $MessageGroup_All->get();
        }
        else
        {
            $MessageGroup_List = $MessageGroup_All->paginate($pageLimit);
        }
            
        return $MessageGroup_List;       
    }

    /**
     * 取得單筆訊息群組By ID
     *
     * @param integer $id
     * @return void
     */
    public function getMessageGroup($id = 0)
    {
        $MessageGroup = MessageGroup::find($id);

        return $MessageGroup;      
    }  

     /**
     * 新增訊息群組
     *
     * @param integer $id
     * @return void
     */
    public function insertMessageGroup(Request $request)
    {
        
        $messageGroup = new MessageGroup;

        $messageGroup->name = $request->name;
        $messageGroup->desc = $request->desc; 
        $messageGroup->valid = 1;       
        $messageGroup->oid = Auth::user()->id;        

        $messageGroup->save();

        $id = $messageGroup->id;

        return $id;
    }

    /**
     * 新增訊息群組名單
     *
     * @param integer $id
     * @return void
     */
    public function insertMessageGroupMember($message_groups_id = 0, $member_id = 0)
    {        
        $messageMember = new MessageMember;     
       
        $messageMember->message_groups_id = $message_groups_id;
        $messageMember->member_id = $member_id;       
        $messageMember->oid = Auth::user()->id;        

        $messageMember->save();

        $id = $messageMember->id;

        return $id;
    }

    /**
     * 取得訊息群組清單By Group_ID
     *  
     * @param integer $message_groups_id
     * @return void
     */
    public function getMessageGroupMember($pageLimit = 0, $message_groups_id = 0)
    {
        $MessageGroupMember_All = MessageMember::select('*'); 
        
        $MessageGroupMember_All->where('message_groups_id', $message_groups_id);
        $MessageGroupMember_All->where('valid', 1);
        //GetAll
        if( $pageLimit == 0 )
        {         
            $MessageGroupMember_List = $MessageGroupMember_All->get();    
        }
        else
        {
            $MessageGroupMember_List = $MessageGroupMember_All->paginate($pageLimit);
        }

        return $MessageGroupMember_List;
    }

    /**
     * 修改訊息群組資料
     *
     * @param integer $id
     * @return void
     */
    public function modifyMessageGroup(Request $request)
    {
        $messageGroup = MessageGroup::find($request->id);
        $messageGroup->name = $request->name;
        $messageGroup->desc = $request->desc; 
        $messageGroup->valid = 1;    
        $messageGroup->oid = Auth::user()->id;        

        $messageGroup->save(); 

        return $request->id;
    }

    /**
     * 刪除訊息群組
     *
     * @param integer $id
     * @return void
     */
    public function deleteMessageGroup($id = 0)
    {
        $messageGroup = MessageGroup::find($id);    
        $messageGroup->valid = 0; 

        $messageGroup->save();
        //$messageGroup->delete();
    }

    /**
     * 刪除指定訊息群組全部名單
     *
     * @param integer $id
     * @return void
     */
    public function deleteMessageGroupMemberByGroup($groups_id = 0)
    {           
        //MessageMember::where('message_groups_id', $groups_id)->delete();
        
        $MessageMember_All = MessageMember::select('*');   
        $MessageMember_All -> where('message_groups_id', $groups_id);
        $MessageMember_All_List = $MessageMember_All -> get();

        foreach($MessageMember_All_List as $MessageMember)
        {
            $Member = MessageMember::find($MessageMember->id);
            $Member->valid = 0;
            $Member->save();
        }
        
    }

    /**
     * 刪除訊息群組名單
     *
     * @param integer $id
     * @return void
     */
    public function deleteMessageGroupMember($id = 0)
    {        
        $messageMember = MessageMember::find($id);  
        $messageMember->valid = 0;
        $messageMember->save();     
        //$messageMember->delete();     
    }
}
