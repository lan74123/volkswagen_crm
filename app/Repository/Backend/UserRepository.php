<?php
namespace App\Repository\Backend;

use App\User;
use Illuminate\Http\Request;
use Auth;

class UserRepository
{
    /**
     * 建構子
     */
    public function __construct()
    {
    }

    /**
     * 取得所有使用者
     *
     * @return User
     */
    public function getAllUsers()
    {
        return User::all();
    }

    /**
     * 取得單一使用者資料
     *
     * @param integer $id
     * @return User
     */
    public function getUser($id = 0)
    {
        return User::find($id);
    }

    /**
     * 更新使用者資料
     *
     * @param Request $request
     * @param integer $id
     * @return bool
     */
    public function modifyUser(Request $request, $id = 0)
    {
        $user = $this->getUser($id);
        $user->name = $request->name;

        if ($request->has('password'))
        {
            $user->password = bcrypt($request->password);
        }

        $user->save();

        return true;
    }

    /**
     * 新增使用者
     *
     * @param Request $request
     * @return integer
     */
    public function insertUser(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);       
        $user->save();

        return $user->id;
    }

    /**
     * 移除使用者
     *
     * @param integer $id
     * @return bool
     */
    public function deleteUser($id = 0)
    {
        $user = $this->getUser($id);
        $user->delete();

        return true;
    }

    /**
     * 取得多個使用者
     *
     * @param array $userIds
     * @return void
     */
    public function getUsers($userIds = array())
    {
        $result = User::whereIn('id', $userIds)->get();

        return $result;
    }

    /**
     * 取得多個使用者以外
     *
     * @param array $userIds
     * @return void
     */
    public function getUsersNotIn($userIds = array())
    {
        $result = User::whereNotIn('id', $userIds)->get();

        return $result;
    }
}