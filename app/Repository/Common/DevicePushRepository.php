<?php

namespace App\Repository\Common;

use Illuminate\Http\Request;
use App\DevicePush;
use Auth;
use App;
use DB;

class DevicePushRepository
{
    /**
     * 取得單筆DevicePushs By ID
     *
     * @param integer $device_id
     * @return void
     */
    public function getDevicePush($device_id = 0)
    {
        $DevicePushs = DevicePush::where('device_id', $device_id)->first();

        return $DevicePushs;      
    }  

     /**
     * 新增DevicePushs
     *
     * @param integer $id
     * @return void
     */
    public function insertDevicePush($device_id = 0, $push_token = null, $device = null)
    {
        $DevicePushs = new DevicePush;
        $DevicePushs->device_id = $device_id;    
        $DevicePushs->push_token = $push_token;
        $DevicePushs->device = $device;        

        $DevicePushs->save();

        return $DevicePushs;
    }

    /**
     * 修改DevicePushs資料
     *
     * @param integer $id
     * @return void
     */
    public function modifyDevicePush($id = 0, $member_id = null, $push_token = null)
    {
        $DevicePushs = DevicePush::find($id);       

        if(!is_null($push_token))
        {
            $DevicePushs->push_token = $push_token;
        }

        if(!is_null($member_id))
        {
            $DevicePushs->member_id = $member_id;
        }

        $DevicePushs->save(); 

        return $DevicePushs;
    }    
}
