<?php

namespace App\Repository\Common;

use Illuminate\Http\Request;
use App\Maintain;
use App\MaintainOrigin;
use Auth;
use App;
use DB;

class MaintainsRepository
{
    /**
     * 複製保養資料
     *
     * @param string $origin_id
     * @param string $vin
     * @param string $license_plate_number
     * @return void
     */
    function copyMaintainsData($origin_id = null, $vin = null, $license_plate_number = null)
    {        
        $maintains_origin = MaintainOrigin::where('origin_id', $origin_id)
        ->where('vin', $vin)->where('license_plate_number', $license_plate_number)->get();


        foreach($maintains_origin as $origin)
        {
            $maintains = new Maintain;
            $maintains->origin_id = $origin->origin_id;
            $maintains->vin = $origin->vin;
            $maintains->license_plate_number = $origin->license_plate_number;
            $maintains->dealer = $origin->dealer;
            $maintains->charge = $origin->charge;
            $maintains->order_details = $origin->order_details;
            $maintains->mileage = $origin->mileage;
            $maintains->close_date = $origin->close_date;
            //$maintains->oid = 0;
            $maintains->save();

            $origin->status = '1';
            $origin->save(); 
        }
        return true;
    }
    
    /**
     * 刪除保養資料
     *
     * @param string $origin_id
     * @param string $vin
     * @param string $license_plate_number
     * @return void
     */
    function deleteMaintainsData($origin_id = null, $vin = null, $license_plate_number = null)
    {        
        $maintains_origin = MaintainOrigin::where('origin_id', $origin_id)
        ->where('vin', $vin)->where('license_plate_number', $license_plate_number)->get();

        foreach($maintains_origin as $origin)
        {
            $origin->status = '0';
            $origin->save();
        }

        $maintains = Maintain::where('origin_id', $origin_id)
        ->where('vin', $vin)->where('license_plate_number', $license_plate_number)->get();

        foreach($maintains as $maintain)
        {
            $maintain->delete();
        }

        return true;
    }   
}
