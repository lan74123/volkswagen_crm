<?php

namespace App\Repository\Common;

use Illuminate\Http\Request;
use App\Member;
use Auth;
use App;
use DB;
use Carbon\Carbon;

class MemberRepository
{

    /**
     * 搜尋多筆會員
     *
     * @param integer $pageLimit
     * @param [type] $name
     * @param [type] $mobile
     * @param string $date_start
     * @param string $date_end
     * @param [type] $valid
     * @return void
     */
    public function searchMembers($pageLimit = 0, $name = null, $mobile = null,  $date_start = '', $date_end = '', $valid = null)
    {        
        $Member_All = Member::select('*');

        if(!is_null($name))
		{			
            $Member_All->where('name', 'LIKE', '%' . $name . '%');
        }
        
        if(!is_null($mobile))
		{			
            $Member_All->where('mobile', 'LIKE', '%' . $mobile . '%');
        }     
        
        if(!empty($date_start))
		{
             $Member_All->where('created_at','>=', $date_start);
		}
        
        if(!empty($date_end))
		{
             $Member_All->where('created_at','<=', $date_end);
		}

        if(!is_null($valid))
		{           
            if($valid != "")//FOR 不拘
            {
                $Member_All->where('valid', $valid);
            }
		}		

        $Member_All->orderBy('id', 'desc')->orderBy('valid', 'desc')->orderBy('agree_data_use', 'desc')->orderBy('is_authenticated', 'desc');

        //GetAll
        if( $pageLimit == 0 )
        {         
            $Member_List = $Member_All->get();
        }
        else
        {
            $Member_List = $Member_All->paginate($pageLimit);
        }
            
        return $Member_List;       
    }

    /**
     * 搜尋多筆會員給群組
     *
     * @param integer $pageLimit
     * @param [type] $name
     * @param [type] $mobile
     * @param string $date_start
     * @param string $date_end
     * @param [type] $valid
     * @return void
     */
    public function searchMembersForGroup($pageLimit = 0, $name = null, $mobile = null, $last_maintenance = null, $car_model = null, $mileage = null,  $date_start = '', $date_end = '')
    {
        $subquery = '(SELECT max(maintains.close_date) from maintains where maintains.vin = cars.vin)';
        $Member_All = Member::join('cars', 'cars.member_id', '=', 'members.id')
        ->join('maintains','maintains.vin','=','cars.vin')->select('members.*')->where('maintains.close_date','=',DB::raw($subquery));

        //dd($Member_All->toSql());

        if(!is_null($name))
		{			
            $Member_All->where('name', 'LIKE', '%' . $name . '%');
        }
        
        if(!is_null($mobile))
		{			
            $Member_All->where('mobile', 'LIKE', '%' . $mobile . '%');
        }  

        if(!is_null($last_maintenance))
		{
            //dd(Carbon::now()->subDays($last_maintenance)->format('Y/m/d'));			
            $Member_All->where('maintains.close_date', '>=', Carbon::now()->subDays($last_maintenance)->format('Y/m/d'));
        }  

        if(!is_null($car_model))
		{			
            $Member_All->where('cars.car_model', 'LIKE', '%' . $car_model . '%');
        }  

        if(!is_null($mileage))
		{			
            $Member_All->where('maintains.mileage','>=', $mileage);
        }     
        
        if(!empty($date_start))
		{
            $Member_All->where('created_at','>=', $date_start);
		}
        
        if(!empty($date_end))
		{
             $Member_All->where('created_at','<=', $date_end);
        }

        //dd($Member_All->toSql());

        // 由於join table所以資料會有重覆 所以使用distinct()使會員資料不重覆
        $Member_All->distinct();
        $Member_All->where('members.valid', 1);
        $Member_All->where('members.agree_data_use', 1);

        $Member_All->orderBy('members.id', 'desc')->orderBy('members.is_authenticated', 'desc');


        //GetAll
        if( $pageLimit == 0 )
        {         
            $Member_List = $Member_All->get();
        }
        else
        {
            $Member_List = $Member_All->paginate($pageLimit);
        }

        return $Member_List;       
    }

    /**
     * 取得單筆會員By ID
     *
     * @param integer $id
     * @return void
     */
    public function getMember($id = 0)
    {
        $Member = Member::find($id);

        return $Member;      
    }  

     /**
     * 新增會員
     *
     * @param integer $id
     * @return void
     */
    public function insertMember(Request $request)
    {
        $member = new Member;
        $member->name = $request->name;
        $member->email = $request->email;        
//        $member->password = bcrypt($request->password);
        $member->mobile = $request->mobile;
        $member->address = $request->address;        
        $member->agree_data_use =  ($request->agree_data_use == 'on') ? 1 : 0;
        $member->authentication_code = 0;
        $member->is_authenticated = 0;
        
        $member->valid = ($request->valid == 'on') ? 1 : 0;
        // dd($member->device_id);
        //判斷為後台操作才寫入操作者ID
        if($request->End == 'backend')
        {
            $Member->oid = Auth::user()->id;
        }

        $member->save();

        $id = $member->id;

        return $id;
    }

    /**
     * 修改會員資料
     *
     * @param integer $id
     * @return void
     */
    public function modifyMember(Request $request)
    {
        $member = Member::find($request->id);
        $member->name = $request->name;
        $member->email = $request->email;          
        // if ($request->password != "")
        // {
        //     $member->password = bcrypt($request->password);
        // }
        $member->mobile = $request->mobile;
        $member->address = $request->address;
        $member->agree_data_use =  ($request->agree_data_use == 'on') ? 1 : 0;
//        $member->authentication_code = $request->authentication_code;
//        $member->is_authenticated = $request->is_authenticated;
        if ($request->device_id != "")
        {
            $member->device_id = $request->device_id;
        }
        $member->valid = ($request->valid == 'on') ? 1 : 0;

        //判斷為後台操作才寫入操作者ID
        if($request->End == 'backend'){
            $Member->oid = Auth::user()->id;
        }
      
        $member->save(); 

        return $request->id;
    }

    /**
     * 刪除會員
     *
     * @param integer $id
     * @return void
     */
    public function deleteMember($id = 0)
    {
        $Member = Member::find($id);  
        $member->valid = 0;
        
        $member->save(); 
       // $Member->delete();
    }


   /**
    * API修改會員資料
    *
    * @param integer $id
    * @param [type] $email
    * @param [type] $mobile
    * @param [type] $address
    * @param [type] $fbid
    * @return void
    */
    public function updateMember($id = 0, $email = null, $mobile = null, $address = null, $fbid = null, $type = null)
    {
        $member = Member::find($id);
        if(!is_null($email))
        {
            $member->email = $email; 
        }
        if(!is_null($mobile))
        {
            $member->mobile = $mobile; 
        }
        if(!is_null($address))
        {
            $member->address = $address; 
        }
        if($type == 'fb')
        {
            $member->fbid = $fbid; 

            if(!is_null($fbid))
            {      
                if ($fbid == '')
                {
                    $member->img_path = '';
                }
                else
                {
                    $member->img_path = 'https://graph.facebook.com/' . $fbid . '/picture?type=large';

                }
            }            
            else
            {
                $member->img_path = '';
            }
        } 
      
        $member->save(); 

        return $member;
    }

    /**
     * API搜尋會員by name
     *
     * @param integer $neme
     * 
     */
    public function searchMemberByName($name = '')
    {   
        $Member_list = Member::where('name', $name)->first();

        return $Member_list;     
    }
    
    /**
     * API會員註冊
     *
     * @param [type] $origin_id
     * @param [type] $name
     * @param [type] $mobile
     * @param [type] $device_id
     * @param [type] $fbid
     * @param integer $agree_data_use
     * @return void
     */
    public function registerMember($origin_id = null, $name = null, $mobile = null, $device_id = null, $fbid = null, $agree_data_use = 0)
    {        
        $member = new Member;
        $member->origin_id = $origin_id;
        $member->name = $name;
        $member->email = "";        
        $member->mobile = $mobile;
        $member->address = "";  
        $member->device_id = $device_id;   
        $member->fbid = $fbid;   
        if(!is_null($fbid))
        {               
            $member->img_path = 'https://graph.facebook.com/' . $fbid . '/picture?type=large';
        }
        else if ($fbid == '')
        {
            $member->img_path = '';
        }
        else
        {
            $member->img_path = '';
        }
        $member->agree_data_use =  1;
        $member->authentication_code = 0;
        $member->is_authenticated = 0;        
        $member->valid = 1;  
       
        $member->save();       

        return $member;     
    }
    
}
