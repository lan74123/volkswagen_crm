<?php

namespace App\Repository\Common;

use Illuminate\Http\Request;
use App\Services as Service;
use App\Helper\ImageHelper;
use App\News;
use Auth;
use Storage;
use Image;
use App;
use DB;

class NewsRepository
{
    protected $fileService;

    /**
     * 建構子
     */
    public function __construct(Service\Common\FileService $fileService)
    {
        $this->fileService = $fileService;       
    }

    /**
     * 搜尋多筆News
     *
     * @param integer $pageLimit
     * @param [type] $title
     * @param string $date_start
     * @param string $date_end
     * @param [type] $is_top
     * @param [type] $valid
     * @return void
     */
    public function searchNews($pageLimit = 0, $title = null, $date_start = '', $date_end = '', $is_top = null, $valid = null)
    {        
        $News_All = News::select('*');

        if(!is_null($title))
		{			
            $News_All->where('title', 'LIKE', '%' . $title . '%');
        }   
        
        if(!empty($date_start))
		{
            $News_All->where('created_at','>=', $date_start);
		}
        
        if(!empty($date_end))
		{
            $News_All->where('created_at','<=', $date_end);
		}

        if(!is_null($is_top))
		{			
            $News_All->where('is_top', $is_top);
        }  

        if(!is_null($valid))
		{           
            if($valid != "")//FOR 不拘
            {
                $News_All->where('valid', $valid);
            }
		}		

        $News_All->orderBy('id', 'desc')->orderBy('valid', 'desc')->orderBy('publish_date', 'desc');

        //GetAll
        if( $pageLimit == 0 )
        {         
            $News_List = $News_All->get();
        }
        else
        {
            $News_List = $News_All->paginate($pageLimit);
        }
            
        return $News_List;       
    }

    /**
     * 取得單筆News By ID
     *
     * @param integer $id
     * @return void
     */
    public function getNews($id = 0)
    {
        $News = News::find($id);

        return $News;      
    }  

     /**
     * 新增News
     *
     * @param integer $id
     * @return void
     */
    public function insertNews(Request $request)
    {
        $News = new News;
        $News->type = $request->type;    
        $News->title = $request->title;
        $News->short_desc = $request->short_desc;        
        $News->content_desc = $request->content_desc;
        $News->info = ($request->info == null) ? "" : $request->info;
        $News->img_path = ($request->img_path == null) ? "" : $request->img_path;        
        $News->publish_date = $request->publish_date;
        $News->url = ($request->url == null) ? "" : $request->url;  
        $News->valid = ($request->valid == 'on') ? 1 : 0;    
        $News->is_top = ($request->is_top == 'on') ? 1 : 0;    
        $News->oid = Auth::user()->id;
       

        $News->save();

        $id = $News->id;

        if ($request->hasFile('img_path'))
        {
            $fileName = $id . '_' . date('YmdHis') . '_' . '.jpg';
            $image_temp = Image::make($request->file('img_path')->getRealPath());
            $image = ImageHelper::resizeImage($image_temp,1400,1049);
            $folder_name = 'news';
            $this->fileService->saveFile($folder_name, $image->stream(), $fileName);

            $News->img_path = $fileName;
            $News->save();
        }

        return $id;
    }

    /**
     * 修改News資料
     *
     * @param integer $id
     * @return void
     */
    public function modifyNews(Request $request)
    {
        $News = News::find($request->id);
        $News->title = $request->title;          
        $News->short_desc = $request->short_desc;
        $News->content_desc = $request->content_desc;
        $News->info = ($request->info == null) ? "" : $request->info;
        $News->publish_date = $request->publish_date;
        $News->url = ($request->url == null) ? "" : $request->url;
        $News->valid = ($request->valid == 'on') ? 1 : 0;
        $News->is_top = ($request->is_top == 'on') ? 1 : 0;

        if ($request->hasFile('img_path'))
        {
            $fileName = $request->id . '_' . date('YmdHis') . '_' . '.jpg';
            $image_temp = Image::make($request->file('img_path')->getRealPath());
            $image = ImageHelper::resizeImage($image_temp,1400,1049);
            $folder_name = 'news';
            $this->fileService->saveFile($folder_name, $image->stream(), $fileName);

            $News->img_path = $fileName;
            $News->save();
        }

        //判斷為後台操作才寫入操作者ID
        if($request->End == 'backend'){
            $News->oid = Auth::user()->id;
        }

        $News->save(); 

        return $request->id;
    }

    /**
     * 刪除News
     *
     * @param integer $id
     * @return void
     */
    public function deleteNews($id = 0)
    {
        $News = News::find($id);  
        $News->valid = 0;
        
        $News->save(); 
       // $News->delete();
    }


   /**
    * 搜尋單筆or多筆已公開News
    *
    * @param integer $pageLimit
    * @param [type] $type
    * @return void
    */
    public function searchPublicNews($pageLimit = 0, $type = null , $NewsID = null)
    {       
        $News_All = News::select(['id','type','title','short_desc','content_desc','info','img_path','url','publish_date']);

        if(!is_null($type))
		{			
            $News_All->where('type', $type);
        }  

        if(!is_null($NewsID))
		{			
            $News_All->where('id', $NewsID);
        }  

        $News_All->where('publish_date','<=', date('Y/m/d'));	
        $News_All->where('valid', 1);
      
        $News_All->orderBy('publish_date', 'desc');

        //GetAll
        if( $pageLimit == 0 )
        {         
            $News_List = $News_All->get();
        }//單筆
        else if( $pageLimit == 1 )
        {         
            $News_List = $News_All->first();
        }
        //首頁兩筆
        else if( $pageLimit == 2 )
        {         
            $News_List = $News_All->limit(2)->get();
        }
        else
        {
            $News_List = $News_All->get();
        }
            
        return $News_List;       
    }

}
