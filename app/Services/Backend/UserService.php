<?php
namespace App\Services\Backend;

use App\Repository as Repo;
use Illuminate\Http\Request;
use App\Presenter\MessagePresenter;

class UserService
{
    //User Repository
    protected $user;

    /**
     * 建構子
     *
     * @param Repo\Backend\UserRepository $user
     */
    public function __construct(Repo\Backend\UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * 取得所有使用者
     *
     * @return void
     */
    public function getAllUsers()
    {
        return $this->user->getAllUsers();
    }

    /**
     * 取得使用者
     *
     * @param integer $id
     * @return void
     */
    public function getUser($id = 0)
    {
        return $this->user->getUser($id);   
    }

    /**
     * 新增使用者
     *
     * @param Request $request
     * @return void
     */
    public function insertUser(Request $request)
    {
        $validateRules = [
            'name' => 'required|max:100',
            'email' => 'required|email|max:200',
            'password' => 'required'
        ];

        $validateMessage = [
            
            'name.required' => MessagePresenter::getRequired('姓名','input'),
            'name.max' => MessagePresenter::getMax('姓名', 100),
            'email.required' => MessagePresenter::getRequired('帳號(Email)','input'),
            'email.max' => MessagePresenter::getMax('帳號(Email)', 200),
            'password.required' => MessagePresenter::getRequired('密碼','input'),
        ];

        $request->validate($validateRules, $validateMessage);

        $userId = $this->user->insertUser($request);

        return $userId;
    }

    /**
     * 修改使用者
     *
     * @param Request $request
     * @param integer $userId
     * @return void
     */
    public function modifyUser(Request $request, $userId = 0)
    {
        $validateRules = [
            'name' => 'required|max:100',
        ];

        $validateMessage = [
            'name.required' => MessagePresenter::getValidatorMessage('name.required'),
            'name.max' => MessagePresenter::getValidatorMessage('name.max', 100),
        ];

        $request->validate($validateRules, $validateMessage);

        $isSuccessful = $this->user->modifyUser($request, $userId);

        return $isSuccessful;
    }

    /**
     * 刪除使用者
     *
     * @param integer $userId
     * @return void
     */
    public function deleteUser($userId = 0)
    {
        $isSuccessful = $this->user->deleteUser($userId);

        return $isSuccessful;
    }

    /**
     * 從群組取得使用者
     *
     * @param [type] $userIds
     * @param boolean $assign
     * @return void
     */
    public function getGroupUsers($userIds = null, $assign = true)
    {
        $userIds = array_values(array_filter(explode(',', $userIds)));

        if ($assign)
        {
            $users = $this->user->getUsers($userIds);
        }
        else
        {
            $users = $this->user->getUsersNotIn($userIds);
        }
        
        return $users;
    }
}