<?php

namespace App\Services\Backend;

use Illuminate\Http\Request;
use App\Repository as Repo;
use App\Presenter\MessagePresenter;
use App;
use Cookie;

class BannerService
{
    //Banner Service
    protected $version;

    /**
     * 建構子
     *
     * @param Repo\Backend\Banner $banner
     */
    public function __construct(Repo\Backend\BannerRepository $banner)
    {
        $this->banner = $banner;
    }

    /**
     * 搜尋多筆經銷商
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchBanner(Request $request, $pageLimit = 0)
    {
        if ( $_SERVER['REQUEST_METHOD'] == "GET" )
        {          
            $request->title = ($request->cookie('b_drs_title')) ? $request->cookie('b_drs_title') : $request->title;
        }
        else
        {
            Cookie::queue('b_drs_title', $request->title, 60);
        }

        $title = $request->title;
       
        return $this->banner->searchBanner($pageLimit, $title);
    }

    /**
     * 取得單筆經銷商
     *
     * @param integer $id
     * @return void
     */
    function getBanner($id = 0)
    {
        return $this->banner->getBanner($id);
    }

    /**
     * 新增經銷商
     *
     * @param Request $request
     * @return void
     */
    function insertBanner(Request $request)
    {
        
        $validateRules = [
            'title' => 'required|max:100',
            'banner' => 'required|image',
        ];

        $validateMessage = [
            'title.required' => MessagePresenter::getValidatorMessage('title.required'),
            'title.max' => MessagePresenter::getValidatorMessage('title.max', 100),
            'banner.required' => MessagePresenter::getValidatorMessage('banner.required'),
            'banner.image' => MessagePresenter::getValidatorMessage('banner.image'),
        ];

        $request->validate($validateRules, $validateMessage);

        return $this->banner->insertBanner($request);
    }

    /**
     * 修改經銷商資料
     *
     * @param Request $request
     * @return void
     */
    function modifyBanner(Request $request)
    {
        $validateRules = [
            'title' => 'required|max:100',
            'banner' => 'image',
        ];

        $validateMessage = [
            'title.required' => MessagePresenter::getValidatorMessage('title.required'),
            'title.max' => MessagePresenter::getValidatorMessage('title.max', 100),
            'banner.image' => MessagePresenter::getValidatorMessage('banner.image'),
        ];

        $request->validate($validateRules, $validateMessage);

        return $this->banner->modifyBanner($request);
    }

    /**
     * 刪除經銷商
     *
     * @param integer $id
     * @return void
     */
    function deleteBanner($id = 0)
    {       
        return $this->banner->deleteBanner($id);
    }
}