<?php
namespace App\Services\Backend;


use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Repository as Repo;
use App\Presenter\MessagePresenter;
use App;
use Cookie;

class StoreService
{
    //經銷商Service
    protected $store;

    /**
     * 建構子
     *
     * @param Repo\Backend\Store $storesitory
     */
    public function __construct(Repo\Backend\StoreRepository $store)
    {
        $this->store = $store;
    }

    /**
     * 搜尋多筆經銷商
     *
     * @param Request $request
     * @param integer $pageLimit
     * @return void
     */
    function searchStore(Request $request, $pageLimit = 0)
    {
        if ( $_SERVER['REQUEST_METHOD'] == "GET" )
        {      
            $request->dealer = ($request->cookie('b_strs_dealer')) ? $request->cookie('b_strs_dealer') : $request->dealer;    
            $request->area = ($request->cookie('b_strs_area')) ? $request->cookie('b_strs_area') : $request->area;
            $request->type = ($request->cookie('b_strs_type')) ? $request->cookie('b_strs_type') : $request->type;
            $request->name = ($request->cookie('b_strs_name')) ? $request->cookie('b_strs_name') : $request->name; 
            $request->tel = ($request->cookie('b_strs_tel')) ? $request->cookie('b_strs_tel') : $request->tel;
            $isValidNull = is_null($request->cookie('b_strs_valid'));

            if( $isValidNull )
            {
                $request->valid = '';
                Cookie::queue('b_strs_valid', $request->valid, 60);
            }
            else
            {
                $request->valid = ( $request->cookie('b_strs_valid') ) ? $request->cookie('b_strs_valid') : $request->valid.'';
            }
        }
        else
        {
            Cookie::queue('b_strs_dealer', $request->dealer, 60);
            Cookie::queue('b_strs_area', $request->area, 60);
            Cookie::queue('b_strs_type', $request->type, 60);
            Cookie::queue('b_strs_name', $request->name, 60);  
            Cookie::queue('b_strs_tel', $request->tel, 60);            
            Cookie::queue('b_strs_valid', $request->valid."", 60);
        }
        
        $area = $request->area;
        $type = $request->type;
        $name = $request->name;
        $tel = $request->tel;    
        $valid = $request->valid;
        $dealer_id=$request->dealer;
       
        return $this->store->searchStores($pageLimit, $dealer_id, $area, $type, $name, $tel, $valid);
    }

    /**
     * 取得單筆經銷商
     *
     * @param integer $id
     * @return void
     */
    function getStore($id = 0)
    {
        return $this->store->getStore($id);
    }

    /**
     * 新增經銷商
     *
     * @param Request $request
     * @return void
     */
    function insertStore(Request $request)
    {
        
        $validateRules = [
            'area' => 'required|max:10',
            'name' => 'unique:stores|required|max:50',
            'code' => 'max:10',
            'tel' => 'max:20',
            'fax' => 'max:20',
            'address' => 'max:100',
            'email' => [
                'max:200', function($attribute, $value, $fail) 
                {
                    $emailArray = explode(',', $value);                   
                    foreach($emailArray as $email)                   
                    {
                        if(!empty($email))
                        {
                            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                            {
                                return $fail(' email: ' . $email . ' is invalid.');
                            }
                        }
                    }
                },
            ], 
            'emailcc' => [
                'max:200', function($attribute, $value, $fail) 
                {
                    $emailccArray = explode(',', $value);                   
                    foreach($emailccArray as $email)                   
                    {
                        if(!empty($email))
                        {
                            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                            {
                                return $fail(' emailcc: ' . $email . ' is invalid.');
                            }
                        }
                    }
                },
            ],         
        ];

        $validateMessage = [
            'area.required' => MessagePresenter::getRequired('服務中心區域','option'),
            'area.max' => MessagePresenter::getMax('服務中心區域', 10),
            'name.required' => MessagePresenter::getRequired('服務中心名稱','text'),
            'name.unique' => MessagePresenter::getUnique('服務中心名稱'),
            'name.max' => MessagePresenter::getMax('服務中心名稱', 50),
            'tel.max' => MessagePresenter::getMax('服務中心連絡方式', 20),
            'fax.max' => MessagePresenter::getMax('服務中心傳真方式', 20),
            'address.max' => MessagePresenter::getMax('服務中心地址', 100),          
            'email.max' => MessagePresenter::getMax('服務中心EMAIL', 200),
            'emailcc.max' => MessagePresenter::getMax('服務中心EMAIL CC', 200),
        ];

        $request->validate($validateRules, $validateMessage);
        
        return $this->store->insertStore($request);
    }

    /**
     * 修改經銷商資料
     *
     * @param Request $request
     * @return void
     */
    function modifyStore(Request $request)
    {
        $validateRules = [
            'area' => 'required|max:10',
            'name' => 'required|max:50',
            'name' =>  Rule::unique('stores')->ignore($request->id, 'id'),
            'code' => 'max:10',
            'tel' => 'max:20',
            'fax' => 'max:20',
            'address' => 'max:100',
            'email' => [
                'max:200', function($attribute, $value, $fail) 
                {
                    $emailArray = explode(',', $value);                   
                    foreach($emailArray as $email)                   
                    {
                        if(!empty($email))
                        {
                            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                            {
                                return $fail(' email: ' . $email . ' is invalid.');
                            }
                        }
                    }
                },
            ], 
            'emailcc' => [
                'max:200', function($attribute, $value, $fail) 
                {
                    $emailccArray = explode(',', $value);                   
                    foreach($emailccArray as $email)                   
                    {
                        if(!empty($email))
                        {
                            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                            {
                                return $fail(' emailcc: ' . $email . ' is invalid.');
                            }
                        }
                    }
                },
            ],           
        ];

        $validateMessage = [
            'area.required' => MessagePresenter::getRequired('服務中心區域','option'),
            'area.max' => MessagePresenter::getMax('服務中心區域', 10),
            'name.required' => MessagePresenter::getRequired('服務中心名稱','text'),
            'name.max' => MessagePresenter::getMax('服務中心名稱', 50),
            'name.unique' => MessagePresenter::getUnique('服務中心名稱'),
            'tel.max' => MessagePresenter::getMax('服務中心連絡方式', 20),
            'fax.max' => MessagePresenter::getMax('服務中心傳真方式', 20),
            'address.max' => MessagePresenter::getMax('服務中心地址', 100),          
            'email.max' => MessagePresenter::getMax('服務中心EMAIL', 200),
            'emailcc.max' => MessagePresenter::getMax('服務中心EMAIL CC', 200), 
        ];

        $request->validate($validateRules, $validateMessage);   
             
        return $this->store->modifyStore($request);
    }

    /**
     * 刪除經銷商
     *
     * @param integer $id
     * @return void
     */
    function deleteStore($id = 0)
    {       
        return $this->store->deleteStore($id);
    }
}