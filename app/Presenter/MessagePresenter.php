<?php
namespace App\Presenter;

class MessagePresenter
{
    /**
     * 取得必填
     *
     * @param string $columnName
     * @param string $type
     * @return string
     */
    public static function getRequired($columnName = null, $type = 'text')
    {
        switch($type)
        {
            case 'text':
                $action = '請輸入';
                break;
            case 'option':
                $action = '請選擇';
                break;
            case 'ckeckbox':
                $action = '請勾選';
                break;
            case 'file':
                $action = '請上傳';
                break;
            default:
                $action = '請輸入';
                break;
        }

        return $action . $columnName;
    }

    /**
     * 取得最多
     *
     * @param string $columnName
     * @param integer $length
     * @return string
     */
    public static function getMax($columnName = null, $length = null)
    {
        return $columnName . '最多' . $length . '字';
    }

    /**
     * 取得數字
     *
     * @param string $columnName
     * @return string
     */
    public static function getInteger($columnName = null)
    {
        return $columnName . '必須是數字';
    }

    /**
     * 判斷唯一
     *
     * @param string $columnName
     * @return string
     */
    public static function getUnique($columnName = null)
    {
        return $columnName . '已重複';
    }

    /**
     * 取得圖片
     *
     * @param string $columnName
     * @return string
     */
    public static function getImage($columnName = null)
    {
        return $columnName . '必須是圖片';
    }

    /**
     * 取得MIME
     *
     * @param string $columnName
     * @param string $fileType
     * @return string
     */
    public static function getMimes($columnName = null, $fileType = null)
    {
        return $columnName . '格式必須是' . $fileType;
    }

    /**
     * 取得日期
     *
     * @param string $columnName
     * @return string
     */
    public static function getDate($columnName = null)
    {
        return $columnName . '必須是日期格式';
    }
}