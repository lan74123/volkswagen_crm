<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::group(['middleware' => 'web', 'prefix' => 'backend'], function () {
    Route::auth();
    Route::group(['middleware' => 'auth', 'namespace' => 'Backend'], function () {
        Route::get('/', ['as' => 'backend.index', 'uses' => 'IndexController@index']);

        Route::post('/logout', function() {
            Auth::logout();
            return redirect('/backend');
        });

        Route::get('/password', 'PasswordController@index');
        Route::post('/password', 'PasswordController@update');

        Route::get('/user', 'UserController@index');
        Route::get('/user/create', 'UserController@create');
        Route::post('/user/store', 'UserController@store');
        Route::get('/user/{id}', 'UserController@edit');
        Route::post('/user/{id}', 'UserController@update');
        Route::delete('/user/{id}', 'UserController@delete');

        Route::get('/menu', 'MenuController@index');
        Route::get('/menu/create', 'MenuController@create');
        Route::post('/menu/store', 'MenuController@store');
        Route::get('/menu/{id}', 'MenuController@edit');
        Route::post('/menu/{id}', 'MenuController@update');
        Route::delete('/menu/{id}', 'MenuController@delete');

        Route::get('/menu/{menuId}/function', 'FunctionController@index');
        Route::get('/menu/{menuId}/function/create', 'FunctionController@create');
        Route::post('/menu/{menuId}/function/store', 'FunctionController@store');
        Route::get('/menu/{menuId}/function/{id}', 'FunctionController@edit');
        Route::post('/menu/{menuId}/function/{id}', 'FunctionController@update');
        Route::delete('/menu/{menuId}/function/{id}', 'FunctionController@delete');

        Route::get('/group', 'GroupController@index');
        Route::get('/group/create', 'GroupController@create');
        Route::post('/group/store', 'GroupController@store');
        Route::get('/group/{id}', 'GroupController@edit');
        Route::post('/group/{id}', 'GroupController@update');
        Route::delete('/group/{id}', 'GroupController@delete');

        Route::get('/news', 'NewsController@index');
        Route::post('/news', 'NewsController@index');
        Route::get('/news/create', 'NewsController@create');
        Route::get('/news/edit/{id}', 'NewsController@edit')->where('id', '[0-9]+');
        Route::post('/news/insert', 'NewsController@insert');
        Route::post('/news/modify', 'NewsController@modify');
        Route::delete('/news/delete/{id}', 'NewsController@delete')->where('id', '[0-9]+');

        Route::get('/member', 'MemberController@index');
        Route::post('/member', 'MemberController@index');
        Route::get('/member/download', 'MemberController@download');
        Route::get('/member/create', 'MemberController@create');
        Route::get('/member/edit/{id}', 'MemberController@edit')->where('id', '[0-9]+');
        Route::post('/member/insert', 'MemberController@insert');
        Route::post('/member/modify', 'MemberController@modify');
        Route::delete('/member/delete/{id}', 'MemberController@delete')->where('id', '[0-9]+');

        Route::post('/member/{id}/insertCar', 'MemberController@insertCar')->where('id', '[0-9]+');
        Route::post('/member/{id}/deleteCar', 'MemberController@deleteCar')->where('id', '[0-9]+');

        Route::get('/car', 'CarController@index');
        Route::post('/car', 'CarController@index');

        Route::get('/dealer', 'DealerController@index');
        Route::post('/dealer', 'DealerController@index');
        Route::get('/dealer/create', 'DealerController@create');
        Route::get('/dealer/edit/{id}', 'DealerController@edit')->where('id', '[0-9]+');
        Route::post('/dealer/insert', 'DealerController@insert');
        Route::post('/dealer/modify', 'DealerController@modify');
        Route::delete('/dealer/delete/{id}', 'DealerController@delete')->where('id', '[0-9]+');

        Route::get('/store', 'StoreController@index');
        Route::post('/store', 'StoreController@index');
        Route::get('/store/{dealerId}', 'StoreController@index')->where('dealerId', '[0-9]+');
        Route::post('/store/{dealerId}', 'StoreController@index')->where('dealerId', '[0-9]+');
        Route::get('/store/{dealerId}/create', 'StoreController@create');
        Route::get('/store/{dealerId}/edit/{id}', 'StoreController@edit')->where('dealerId', '[0-9]+')->where('id', '[0-9]+');
        Route::post('/store/insert', 'StoreController@insert');
        Route::post('/store/{dealerId}/modify', 'StoreController@modify')->where('dealerId', '[0-9]+');
        Route::delete('/store/delete/{id}', 'StoreController@delete')->where('id', '[0-9]+');

        Route::get('/messageGroup', 'MessageGroupController@index');
        Route::post('/messageGroup', 'MessageGroupController@index');            
        Route::get('/messageGroup/filterMemberList', 'MessageGroupController@filterMemberList');
        Route::post('/messageGroup/filterMemberList', 'MessageGroupController@filterMemberList');
        Route::get('/messageGroup/create', 'MessageGroupController@create');
        Route::post('/messageGroup/create', 'MessageGroupController@create');
        Route::get('/messageGroup/edit/{id}', 'MessageGroupController@edit')->where('id', '[0-9]+');
        Route::get('/messageGroup/download/{id}', 'MessageGroupController@download')->where('id', '[0-9]+');
        Route::post('/messageGroup/insert', 'MessageGroupController@insert');
        Route::post('/messageGroup/modify', 'MessageGroupController@modify');
        Route::delete('/messageGroup/delete/{id}', 'MessageGroupController@delete')->where('id', '[0-9]+');
        Route::delete('/messageGroup/deleteGroupMember/{groupId}/{id}', 'MessageGroupController@deleteMember')->where('groupId', '[0-9]+')->where('id', '[0-9]+');        

        Route::get('/message', 'MessageController@index');
        Route::post('/message', 'MessageController@index');     
        Route::get('/message/create', 'MessageController@create'); 
        Route::post('/message/insert', 'MessageController@insert');   
        Route::get('/message/edit/{id}', 'MessageController@edit')->where('id', '[0-9]+');   
        Route::delete('/message/delete/{id}', 'MessageController@delete')->where('id', '[0-9]+');
        Route::post('/message/resend/{id}', 'MessageController@resend')->where('id', '[0-9]+');

        Route::get('/campaign', 'CampaignController@index');
        Route::get('/campaign/create', 'CampaignController@create');
        Route::post('/campaign/store', 'CampaignController@store');
        Route::get('/campaign/{id}', 'CampaignController@edit');
        Route::post('/campaign/{id}', 'CampaignController@update');
        Route::delete('/campaign/{id}', 'CampaignController@delete');

        Route::get('/banner', 'BannerController@index');
        Route::post('/banner', 'BannerController@index');
        Route::get('/banner/create', 'BannerController@create');
        Route::get('/banner/edit/{id}', 'BannerController@edit')->where('id', '[0-9]+');
        Route::post('/banner/insert', 'BannerController@insert');
        Route::post('/banner/modify', 'BannerController@modify');
        Route::delete('/banner/delete/{id}', 'BannerController@delete')->where('id', '[0-9]+');

        Route::get('/version', 'VersionController@index');
        Route::post('/version', 'VersionController@index');
        Route::get('/version/create', 'VersionController@create');
        Route::get('/version/edit/{id}', 'VersionController@edit');
        Route::post('/version/insert', 'VersionController@insert');
        Route::post('/version/modify', 'VersionController@modify');
        Route::delete('/version/delete/{id}', 'VersionController@delete')->where('id', '[0-9]+');

        
    });
});

/*API路由*/
 Route::group(['middleware' => 'jwtcheck', 'prefix' => 'api', 'namespace' => 'Api'], function () 
 {
    Route::group(['middleware' => ['jwt']], function () 
    {       
        Route::group(['middleware' => ['auth:api','cors']], function () 
        {       
            Route::get('/test', 'ApiController@test');
                                
            Route::post('/APP_Sync_Owner_Data', 'ApiController@MemberLogin');
            Route::post('/APP_Get_Owner_Data', 'ApiController@GetOwnerData');
            Route::post('/APP_Update_Owner_Data', 'ApiController@UpdateOwnerData');            
            Route::post('/APP_Main', 'ApiController@APPMain');
            Route::post('/APP_FBID', 'ApiController@UpdateFBID');
            Route::post('/APP_Get_Main_Data', 'ApiController@GetMainData');
            Route::post('/APP_Add_Car_Data', 'ApiController@AddCarData');
            Route::post('/APP_Del_Car_Data', 'ApiController@DelCarData');
            Route::post('/APP_Get_Messages_Detail', 'ApiController@GetMessagesDetail'); 
        });
    });
});


    //route for testing jwt
Route::group(['prefix' => 'api', 'namespace' => 'Api', 'middleware'  => 'cors'], function ()
{
        Route::post('/login','ApiController@login');
        Route::get('/APP_Version', 'ApiController@Version');
        Route::post('/APP_Sign_Up', 'ApiController@SignUp');         
        Route::post('/APP_Get_News_Data', 'ApiController@GetNewsData');
        Route::post('/APP_Get_News_Detail_Data', 'ApiController@GetNewsDetailData');           
        Route::post('/APP_Weather', 'ApiController@Weather');   
        Route::post('/APP_Push_Token', 'ApiController@PushToken');
});