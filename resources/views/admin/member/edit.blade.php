@extends('admin.master')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<form id="EditForm" class="form-horizontal" method="post" action="{{ asset('/backend/member/modify') }}">
				{{ csrf_field() }}
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h4 class="panel-title">Member Edit</h4>
					</div>
					<div class="panel-body">
						<div>
							<input name="id" type="readonly" value="{{ $member->id }}" id="id"  style="display:none">
							<!-- 表格本體 -->
							<table class="table" cellspacing="0" id="DetailsView1" style="border-collapse:collapse;">
								<tbody>									
									<!-- 欄位：MemberName -->
									<tr>
										<td class="header-require col-lg-2"><span style="color:red">*</span>姓名</td>										
										<td>
											<div class="col-lg-4 nopadding">											
												<input name="name" type="text" value="{{ $member->name }}" readonly maxlength="20" id="name" class="form-control">											
												<label class="error" for="name"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：UserEmail -->								
									<tr>
										<td class="header-require col-lg-2"><span style="color:red">*</span>帳號</td>
										<td>
											<div class="col-lg-4 nopadding">
												<input name="email" type="text" value="{{ $member->email }}" readonly maxlength="100" id="email" class="form-control">
												<label class="error" for="email"></label>
											</div>
										</td>
									</tr>								
									
									<!-- 欄位：mobile -->
									<tr>
										<td class="header-require col-lg-2">連絡電話</td>
										<td>
											<div class="col-lg-4 nopadding">
												<input name="mobile" type="text" value="{{ $member->mobile }}" maxlength="20" id="mobile" class="form-control">
												<label class="error" for="mobile"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：address -->
									<tr>
										<td class="header-require col-lg-2">連絡地址</td>
										<td>
											<div class="col-lg-5 nopadding">
												<input name="address" type="text" value="{{ $member->address }}" maxlength="20" id="address" class="form-control">
												<label class="error" for="address"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：device_id -->
									<tr>
										<td class="header-require col-lg-2">Device token</td>
										<td>
											<div class="col-lg-5 nopadding">
													<input name="device_id" type="text" readonly value="{{ $member->device_id }}" id="device_id" class="form-control">
													<label class="error" for="device_id"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：authentication_code -->
									<tr>
										<td class="header-require col-lg-2">驗證碼</td>
										<td>
											<div class="col-lg-5 nopadding">
													<input name="authentication_code" type="text" readonly value="{{ $member->authentication_code }}" id="authentication_code" class="form-control">
													<label class="error" for="authentication_code"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：is_authenticated -->
									<tr>
										<td class="header-require col-lg-2">是否驗證</td>
										<td>
											<div class="col-lg-5 nopadding">
													<input type="checkbox" name="is_authenticated" {{ ($member->is_authenticated == '1')?'checked':'' }} disabled >
													<label class="error" for="is_authenticated"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：agree_data_use -->
									<tr>
										<td class="header-require col-lg-2">同意資料使用</td>
										<td>
											<div class="col-lg-5 nopadding">
												<input type="checkbox" name="agree_data_use" {{ ($member->agree_data_use == '1')?'checked':'' }}>
												<label class="error" for="agree_data_use"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：agree_data_use -->
									<tr>
										<td class="header-require col-lg-2">是否有效</td>
										<td>
											<div class="col-lg-5 nopadding">
												<input type="checkbox" name="valid" {{ ($member->valid == '1')?'checked':'' }}>
												<label class="error" for="valid"></label>
											</div>
										</td>
									</tr>									
									<!-- 下控制按鈕 -->
									<tr>
										<td>&nbsp;</td>
										<td>
											<div style="text-align: right">		
												<a class="btn btn-xs btn-default" href="{{ asset('/backend/member') }}">back</a>									
												<input type="submit" name="btnUpdate_foot" value="update" id="btnUpdate_foot" class="btn btn-primary btn-xs" onclick="submitForm();">
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- panel-body -->
				</div>
			</form>

			<!-- 車輛少於三筆新增按鈕 -->
			@if(count($mambercars)<3)
			<form id="" class="form-horizontal" method="post" action="{{ asset('/backend/member/'. $member->id .'/insertCar') }}">
				{{ csrf_field() }}
				<div class="panel panel-primary">					
					<div class="panel-body">
						<div>							
							<a class="btn btn-primary" data-toggle="collapse" href="#addMyCar" role="button" aria-expanded="false" aria-controls="addMyCar">新增車輛</a>
							<input name="name" type="readonly" value="{{ $member->name }}" maxlength="20"  style="display:none">
							<div class="row">
								<div class="col">
									  <div class="collapse multi-collapse" id="addMyCar">
											<table class="table" cellspacing="0" id="DetailsView1" style="border-collapse:collapse;">
												<tbody>									
													<!-- 欄位：MemberName -->
													<tr>
														<td class="header-require col-lg-1"><span style="color:red">*</span>車輛VIN碼(後五碼)</td>										
														<td class="col-lg-3">
															<div class="nopadding">											
																<input name="VIN" type="text" value="{{old('VIN')}}" maxlength="5" id="VIN" class="form-control">
															</div>
														</td>														
														<td class="header-require col-lg-1"><span style="color:red">*</span>車牌號碼</td>
														<td class="col-lg-3 ">
															<div class=" nopadding">
																<input name="LicensePlateNumber" type="text" value="{{old('LicensePlateNumber')}}" maxlength="10" id="LicensePlateNumber" class="form-control">																	
															</div>
														</td>
														<td class="header-require col-lg-6">
															<input type="submit" name="" value="add" id="addCar" class="btn btn-darkblue btn-xs" >
														</td>
													</tr>
												</tbody>
											</table>
									  </div>
									</div>									
								</div>
						</div>
					</div>
					<!-- panel-body -->
				</div>
			</form>
			@endif

			<!-- 車輛少於三筆新增按鈕 -->
			<ul class="nav nav-tabs nav-justified">
				@foreach($mambercars as $key => $car)	
					@if($key == 0)				
						<li class="active">
					@else
						<li class="">
					@endif
						<a data-toggle="tab" href={{ '#car' . $key  }}>{{$car->license_plate_number}}</a></li>	
				@endforeach				
			</ul>
			
			<div class="tab-content">
					@foreach($mambercars as $key => $car)	
						@if($key == 0)				
								<div id={{ 'car' . $key  }} class="tab-pane fade in active">
						@else
								<div id={{ 'car' . $key  }} class="tab-pane fade ">
						@endif

						<form id={{ 'formcar' . $key  }} class="form-horizontal" method="post" action="{{ asset('/backend/member/'. $member->id .'/deleteCar') }}">
						{{ csrf_field() }}
							<input name="name" type="readonly" value="{{ $member->name }}" maxlength="20"  style="display:none">
							<input name="VIN" type="readonly" value="{{ $car->vin }}" maxlength="20"   style="display:none">
							<input name="LicensePlateNumber" type="readonly" value="{{ $car->license_plate_number }}" maxlength="20"  style="display:none">
														
							<h3>保養紀錄</h3><input type="submit" name="" value="delete car" id={{ 'cardelete' . $key  }} onclick="return confirm('即將刪除此筆車輛資料，確認繼續刪除?')" class="btn btn-danger btn-xs float-right" >
						</form>

						@if(count($car->maintains)>0)
						<table class="table" cellspacing="0" id={{'maintain' .  $key}} style="border-collapse:collapse;">
									<tbody>									
										<th>維修日期</th>
										<th>里程數</th>
										<th>總金額</th>
										<th>經銷商</th>
										@foreach($car->maintains as $maintain)	
										<tr>
											<td class="header-require col-lg-3">{{ $maintain->close_date}}</td>	
											<td class="header-require col-lg-3">{{ $maintain->mileage}}</td>
											<td class="header-require col-lg-3">{{ $maintain->charge}}</td>
											<td class="header-require col-lg-3">{{ $maintain->dealer}}</td>
										</tr>
										@endforeach
										
									</tbody>
								</table>
							@endif
						
						</div>						
					@endforeach
			</div>
		</div>
	</div>
@endsection

@section('extjs')
	<script>
	$(document).ready(function() 
	{

		//初始化需要偵錯的表格
		$('#EditForm').validate();
		//正規表達驗證初始化
		$.validator.addMethod(
			"regex",
			function (value, element, regexp) 
			{
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			}
		);
		//各欄位
		$('#name').rules("add", 
		{
			required: true,
			minlength: 1,
			maxlength: 20,
			messages: {
				required: "name length must between 1-20",
				minlength: "name length must between 1-20",
				maxlength: "name length must between 1-20"
			}
		});
		
		//Update Mode
		$('#email').rules("add", 
		{
			required: true,
			email: true,
			minlength: 1,
			maxlength: 100,
			messages: {
				required: "Username length must between 1-100",
				email: "Username must be an email address",
				minlength: "Username length must between 1-100",
				maxlength: "Username length must between 1-100"
			}
		});
		
	});
	//提交與取消按鈕
	function submitForm() 
	{
		if (!!($("#EditForm").valid()) === false) 
		{
			return false;
		} 
		else 
		{
			$(document).ready(function() 
			{
				$.blockUI({ css: 
				{
					border: 'none',
					padding: '15px',
					backgroundColor: '#000',
					'-webkit-border-radius': '10px',
					'-moz-border-radius': '10px',
					opacity: .5,
					color: '#fff'
				}});
			});
		}
	}

	function cancelValidate() 
	{
		$("#EditForm").validate().cancelSubmit = true;
	}
	</script>
@endsection
