@extends('admin.master')
@inject('BackendPresenter', 'App\Presenter\BackendPresenter')

@section('content')
    <script src="{{URL::asset('assets/js/jquery.MultiFile.js')}}"></script>
    <div class="row">
        <div class="col-md-12">
            <form id="EditForm" class="form-horizontal" method="post" action="{{ asset('/backend/news/insert') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">新增 Add</h4>
                    </div>
                    <div class="panel-body">
                        <div>
                            <!-- 表格本體 -->
                            <table class="table" cellspacing="0" id="DetailsView1" style="border-collapse:collapse;">
                                <tbody>									
                                    <!-- 欄位：NewsType -->
                                    <tr>
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>類別</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">											
                                                <select id="type" name="type" class="form-control">
                                                    <option value="1" {{ (old('type') == "1") ? 'selected' : '' }}>消息 News</option>
                                                    <option value="2" {{ (old('type') == "2") ? 'selected' : '' }}>活動 Event</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：NewsTitle -->
                                    <tr>
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>名稱</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">											
                                                <input name="title" type="text" value="{{old('title')}}" maxlength="50" id="title" class="form-control">											
                                                <label class="error" for="title"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：NewsShortDesc -->
                                    <tr>
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>短描述</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">
                                                <textarea name="short_desc" type="text" value="" maxlength="200" id="short_desc"  rows="5" class="form-control">{{old('short_desc')}}</textarea>											
                                                <label class="error" for="short_desc"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：NewsDesc -->
                                    <tr>
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>內容簡介</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">
                                                <textarea name="content_desc" type="text" value="" maxlength="500" id="content_desc"  rows="10" class="form-control">{{old('content_desc')}}</textarea>										
                                                <label class="error" for="content_desc"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：Info -->
                                    <tr id="forEvent" style="display:none;">
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>活動資訊</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">
                                                    <textarea name="info" type="text" value="" maxlength="300" id="info"  rows="5" class="form-control">{{old('info')}}</textarea>																						
                                                <label class="error" for="info"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：NewsFile -->
                                    <tr>
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>圖片上傳</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">											
                                                <input type="file" name="img_path" class="multi with-preview maxsize-3072" accept="png|jpeg|jpg" />
                                                JPG格式 1400*1049，大小不得超過3MB / JPG 1400*1049 Size under 3MB
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：NewsPublishDate -->
                                    <tr>
                                        <td class="header-require col-lg-2"><span style="color:red">*</span>上架日期</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">
                                                <div class="input-group">											
                                                    <input name="publish_date" type="text" value="{{old('publish_date')}}" id="publish_date" class="form-control">	
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>										
                                                    <label class="error" for="publish_date"></label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：NewsUrl -->
                                    <tr>
                                        <td class="header-require col-lg-2">外連網址</td>
                                        <td>
                                            <div class="col-lg-10 nopadding">											
                                                <input name="url" type="text" value="" maxlength="200" rows="2" id="url" class="form-control">											
                                                <label class="error" for="url"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- 欄位：valid -->
                                    <tr>
                                        <td class="header-require col-lg-2">是否有效</td>
                                        <td>
                                            <div class="col-lg-5 nopadding">
                                                <input type="checkbox" name="valid" checked>
                                                <label class="error" for="valid"></label>
                                            </div>
                                        </td>
                                    </tr>	
                                    <!-- 欄位：is_top -->
                                    <tr>
                                        <td class="header-require col-lg-2">是否置頂</td>
                                        <td>
                                            <div class="col-lg-5 nopadding">
                                                <input type="checkbox" name="is_top" checked>
                                                <label class="error" for="is_top"></label>
                                            </div>
                                        </td>
                                    </tr>									
                                    <!-- 下控制按鈕 -->
									<tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <div style="text-align: right">			
                                                <a class="btn btn-xs btn-default" href="{{ asset('/backend/news') }}">back</a>								
                                                <input type="submit" name="btnUpdate_foot" value="create" id="btnUpdate_foot" class="btn btn-primary btn-xs" onclick="submitForm();">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- panel-body -->
                </div>
            </form>
        </div>
    </div>
@endsection

@section('extjs')
	<script>
	$(document).ready(function() 
	{		
		//初始化需要偵錯的表格
		$('#EditForm').validate();
		//正規表達驗證初始化
		$.validator.addMethod(
			"regex",
			function (value, element, regexp) 
			{
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			}
		);
		
		//各欄位
		$('#title').rules("add", 
		{
			required: true,
			minlength: 1,
			maxlength: 50,
			messages: {
				required: "title length must between 1-50",
				minlength: "title length must between 1-50",
				maxlength: "title length must between 1-50"
			}
		});		
		
        $("#type").change(function () {
            if ($("#type").val() == "2")
                $("[id*=forEvent]").removeAttr("style");
            else
                $("[id*=forEvent]").attr("style","display:none;");
		});

        $(function () {
            $("#publish_date").datepicker({ "dateFormat": "yy/mm/dd" });
        });   
	});
	//提交與取消按鈕
	function submitForm() 
	{
		if (!!($("#EditForm").valid()) === false) 
		{
			return false;
		} 
		else 
		{
			$(document).ready(function() 
			{
				$.blockUI({ css: {
					border: 'none',
					padding: '15px',
					backgroundColor: '#000',
					'-webkit-border-radius': '10px',
					'-moz-border-radius': '10px',
					opacity: .5,
					color: '#fff'
				}});
			});
		}
	}

	function cancelValidate() 
	{
		$("#EditForm").validate().cancelSubmit = true;
	}
	</script>
@endsection
