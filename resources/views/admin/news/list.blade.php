@extends('admin.master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-dark">
                <div class="panel-heading">
                    <div class="row">
                        <div class="form-group">
                            <div class="panel-title">Manage News/Event</div>
                        </div>
                        <div class="form-inline">
                            <form method="post" action="{{ asset('/backend/news') }}" name="searchFrom">
                                {{ csrf_field() }}
                                <div class="form-group" >
                                    <label class="control-label" style="color:white;">標題 <br/>News Title:</label><input type="text" name="title" value="{{ $cond->title }}"  class="form-control">
                                </div>
                                <div class="form-group" >
                                    <label class="control-label" style="color:white;">起始日期 <br/>Start Date:</label><input type="text" value="{{ $cond->date_start }}" id="date_start" name="date_start" class="form-control">
                                </div>
                                <div class="form-group" >
                                    <label class="control-label" style="color:white;">結束日期 <br/>End Date:</label><input type="text" value="{{ $cond->date_end }}" id="date_end" name="date_end" class="form-control">
                                </div>
                                <div class="form-group" >
                                    <label class="control-label" style="color:white;">是否置頂 <br/>Show On Top:</label>
                                    <select name="is_top" class="form-control">
                                        <option value="" {{ ($cond->is_top == "") ? 'selected' : '' }}>不拘</option>
                                        <option value="1" {{ ($cond->is_top == "1") ? 'selected' : '' }}>是 Yes</option>
                                        <option value="0" {{ ($cond->is_top == "0") ? 'selected' : '' }}>否 No</option>
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label class="control-label" style="color:white;">是否有效 <br/>Status:</label>
                                    <select name="valid" class="form-control">
                                        <option value="1" {{ ($cond->valid == "1") ? 'selected' : '' }}>有效 Valid</option>
                                        <option value="0" {{ ($cond->valid == ""||$cond->valid == "0") ? 'selected' : '' }}>無效 Invalid</option>
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <a id="btnOK" name="btnOK"  class="btn btn-danger btn-xs" >Search</a>
                                    <a class="btn btn-darkblue btn-xs" href="{{ asset('backend/news/create') }}"><strong>Add</strong></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <table class="table table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>類別 Cate</td>
                        <td>標題 Title</td>
                        <td>上架日期 Publish Date</td>
                        <td>是否置頂 Show On Top</td>
                        <td>是否有效 Status</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tables as $data)
                        <tr>
                            <td>
                                {{ $data->id }}
                            </td>
                            <td>
                                {{ $data->type == "1" ? "消息 News" : "活動 Event" }}
                            </td>
                            <td>
                                {{ $data->title }}
                            </td>
                            <td>
                                {{ substr($data->publish_date,0,10)}}
                            </td>
                            <td>
                                {{ $data->is_top == "1" ? "是 Yes" : "否 No" }}
                            </td>
                            <td>
                                {{ $data->valid == "1" ? "有效 Valid" : "無效 Invalid" }}
                            </td>
                            <td style="text-align: right">
                                <form method="post" action="{{    url('/backend/news/delete/'.$data->id) }}">
                                    <a class="btn btn-xs btn-success" href="{{ asset('/backend/news/edit/'.$data->id) }}">Edit</a>
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    @if ($data->valid == 1)
                                        <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm('確定要將此筆資料更改為無效?\nChange this data to invalid ?');">
                                            <strong>Invalid</strong>
                                        </button>
                                    @endif
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="col-md-12 text-center no-margin">
                    <?php echo $tables->render(); ?>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extjs')
    <script>
        $(function () {
            $("#date_start").datepicker({ "dateFormat": "yy-mm-dd" });
            $("#date_end").datepicker({ "dateFormat": "yy-mm-dd" });

            //判斷活動開始結束日期
            $("#btnOK").on("click", function () {
                if(checkDate()){
                    $('form[name="searchFrom"]').submit();
                }
            });

        });

        //判斷開始日期不得大於結束日期
        function  checkDate() {
            var beginDate=$("#date_start").val();
            var endDate=$("#date_end").val();
            var d1 = new Date(beginDate.replace(/-/g, "/"));
            var d2 = new Date(endDate.replace(/-/g, "/"));

            if(beginDate!=""&&endDate!=""&&d1 >d2)
            {
                alert("開始日期不能晚於結束日期！\n The start date can not be later than the end date !");
                return false;
            }else{
                return true;
            }
        }
    </script>
@endsection
