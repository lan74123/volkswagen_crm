@extends('admin.master')
@inject('BackendPresenter', 'App\Presenter\BackendPresenter')

@section('content')
	<div class="row">
		<div class="col-lg-12">
			<form id="EditForm" class="form-horizontal" method="post" action="{{ asset('/backend/version/insert') }}">
				{{ csrf_field() }}
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h4 class="panel-title">Version Add</h4>
					</div>
					<div class="panel-body">
						<div>
							<!-- 表格本體 -->
							<table class="table" cellspacing="0" id="DetailsView1" style="border-collapse:collapse;">
								<tbody>	
									<!-- 欄位：version -->
									<tr>
										<td class="header-require col-lg-2"><span style="color:red">*</span>版本號</td>
										<td>
											<div class="col-lg-4 nopadding">											
												<input name="version" type="text"  maxlength="30" id="version" class="form-control" value="{{ old('version')}}">											
												<label class="error" for="version"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：Device -->
									<tr>
										<td class="header-require col-lg-2">裝置</td>
										<td>
											<div class="col-lg-4 nopadding">
												<select id="device" name="device" class="form-control">
													<option value="ios">iOS</option>
													<option value="android">Android</option>
												</select>									
												<label class="error" for="device"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：is Update -->
									<tr>
										<td class="header-require col-lg-2">是否強制更新</td>
										<td>
											<div class="col-lg-5 nopadding">
												<input type="checkbox" name="is_update">
												<label class="error" for="is_update"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：Message -->
									<tr>
										<td class="header-require col-lg-2">訊息</td>
										<td>
											<div class="col-lg-8 nopadding">
												<input name="message" type="text"  maxlength="100" id="message" class="form-control" value="{{ old('message')}}">
												<label class="error" for="message"></label>
											</div>
										</td>
									</tr>
									<!-- 欄位：url -->								
									<tr>
										<td class="header-require col-lg-2"><span style="color:red">*</span>URL</td>
										<td>
											<div class="col-lg-4 nopadding">
												<input name="url" type="text"  maxlength="100" id="url" class="form-control" value="{{ old('url')}}">
												<label class="error" for="url"></label>
											</div>
										</td>
									</tr>						
									<!-- 下控制按鈕 -->
									<tr>
										<td>&nbsp;</td>
										<td>
											<div style="text-align: right">			
												<a class="btn btn-xs btn-default" href="{{ asset('/backend/version') }}">Back</a>								
												<input type="submit" name="btnUpdate_foot" value="create" id="btnUpdate_foot" class="btn btn-primary btn-xs" onclick="submitForm();">
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- panel-body -->
				</div>
			</form>
		</div>
	</div>
@endsection

@section('extjs')
	<script>
	$(document).ready(function() 
	{		
		//初始化需要偵錯的表格
		$('#EditForm').validate();
		//正規表達驗證初始化
		$.validator.addMethod(
			"regex",
			function (value, element, regexp) 
			{
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			}
		);
		
		//各欄位
		$('#name').rules("add", 
		{
			required: true,
			minlength: 1,
			maxlength: 20,
			messages: {
				required: "name length must between 1-20",
				minlength: "name length must between 1-20",
				maxlength: "name length must between 1-20"
			}
		});		
		
	});
	//提交與取消按鈕
	function submitForm() 
	{
		if (!!($("#EditForm").valid()) === false) 
		{
			return false;
		} 
		else 
		{
			$(document).ready(function() 
			{
				$.blockUI({ css: {
					border: 'none',
					padding: '15px',
					backgroundColor: '#000',
					'-webkit-border-radius': '10px',
					'-moz-border-radius': '10px',
					opacity: .5,
					color: '#fff'
				}});
			});
		}
	}

	function cancelValidate() 
	{
		$("#EditForm").validate().cancelSubmit = true;
	}
	</script>
@endsection
